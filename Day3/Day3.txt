/MNIT_IIITK_FAST_TRACK_ANDROID_DAY_3    
###################################################################################
GUI Coding Part 2 ( ViewGroups )
####################################################################

1. To arrange or position multiple views on one screen we require ViewGroups
	All popular apps have multiple views 
	Uber, Inbox by Gmail, Charity Miles, Signtist
	
1. We need to have a single root view to contain multiple views
	This root view is a rectangular view also known as ViewGroup
	Its a container for views.
	So concept of Parent and Child Views and Siblings views
	
1. So view groups have width, height, Background color

1. <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="200dp"
    android:layout_height="400dp"
    android:background="@android:color/holo_blue_bright">
	
1. Google android orientation 
	change the orientation from vertical to horizontal to experiment

# Add these three layout to show the orientation
1. <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="Ram"
        android:textSize="40sp" />

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="Shyam"
        android:textSize="40sp" />

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="Lakshman"
        android:textSize="40sp" />

1. android:orientation="vertical"

1. android:orientation="horizontal"


	
1. 	Experiment with LinearLayout 

1. Differentiate between 
	fixed width/height in dp
	wrap content ( content gets large the view gets large)
	match parent ( view will be as wide/tall as the parent )
	
	to understand the concept we should always define the bg color for view/groups
	For one LinearLayout and multiple TextViews for Guest List example
	
	All the attribute which has layout_  is being handled by the parent for positing 
	
1. 	Taking advantage of screen real estate
	
	3 TextViews with LinearLayout with 3 different bg color
	One of the solution is to fix for a specific device using fixed width and height
	Also try match_parent
	
	linear layout equally spaced children on google and refer to the stack overflow
	
	layout weight in the google again and let students read it
	
	set the layout_height="0dp"
	layout_weight="1"
	layout weight sum logic also

<TextView
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:background="@android:color/darker_gray"
        android:text="Ram"
        android:textSize="40sp" />

    <TextView
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:background="@android:color/darker_gray"
        android:text="Shyam"
        android:textSize="40sp" />

    <TextView
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:background="@android:color/darker_gray"
        android:text="Lakshman"
        android:textSize="40sp"

        />
	 
1. Introduce the concept of Relative Layout with Parent

1. <!--left-->
   <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="BirthDay"
        android:textSize="40sp"
        android:layout_alignParentLeft="true"
        android:layout_alignParentTop="true"
        android:layout_alignParentRight="false"
        android:layout_alignParentBottom="false"/>

1.  <!-- Right-->
	 <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="BirthDay"
        android:textSize="40sp"
        android:layout_alignParentLeft="false"
        android:layout_alignParentTop="true"
        android:layout_alignParentRight="true"
        android:layout_alignParentBottom="false"/>

1. <!--bottom/Right-->
<TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="BirthDay"
        android:textSize="40sp"
        android:layout_alignParentLeft="false"
        android:layout_alignParentTop="false"
        android:layout_alignParentRight="true"
        android:layout_alignParentBottom="true"/>

1. <!--Bottom/Left-->
<TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="BirthDay"
        android:textSize="40sp"
        android:layout_alignParentLeft="true"
        android:layout_alignParentTop="false"
        android:layout_alignParentRight="false"
        android:layout_alignParentBottom="true"/>

1 <!--Bottom/Center Horizontal-->
<TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="BirthDay"
        android:textSize="40sp"
        android:layout_centerHorizontal="true"
        android:layout_alignParentLeft="false"
        android:layout_alignParentTop="false"
        android:layout_alignParentRight="false"
        android:layout_alignParentBottom="true"/>

1. <!-- Left/Center Vertical-->
<TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="BirthDay"
        android:textSize="40sp"
        android:layout_centerVertical="true"
        android:layout_alignParentLeft="false"
        android:layout_alignParentTop="false"
        android:layout_alignParentRight="false"
        android:layout_alignParentBottom="false"/>


1. Introduce the concept of Relative Layout with Siblings

<!-- Base Layout -->
<TextView
        android:id="@+id/ram"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="Ram"
        android:textSize="40sp"
        android:layout_centerHorizontal="true"/>

    <TextView
        android:id="@+id/sejal"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="Sejal"
        android:textSize="40sp"
        android:layout_alignParentBottom="true"
        android:layout_alignParentLeft="true"/>

    <TextView
        android:id="@+id/ritu"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="@android:color/darker_gray"
        android:text="Ritu"
        android:textSize="40sp" 
        android:layout_alignParentBottom="true"
        android:layout_alignParentRight="true"/>

1. Introduce the concept of padding and margin

1. android:padding="40dp"
        android:paddingLeft="40dp"
        android:paddingTop="40dp"
        android:paddingRight="40dp"
        android:paddingBottom="40dp"

1. android:layout_margin="40dp"
        android:layout_marginLeft="40dp"
        android:layout_marginTop="40dp"
        android:layout_marginRight="40dp"
        android:layout_marginBottom="40dp"

1. Introduce the concept of keyline and Material Design Concept


1. Give classwork for making 3 screens starting from simple to complex UI 


1. Give the concept of Responsive Design Thinking

