//MNIT_IIITK_FAST_TRACK_ANDROID_DAY_7
      
    
1. Downloading image from internet

    Step 0: copy place_holder.png file from google drive to your drawable folder.
   
	Step 1:  Copy the following code in main activity xml
        <?xml version="1.0" encoding="utf-8"?>
   		<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    		xmlns:tools="http://schemas.android.com/tools"
    		android:layout_width="match_parent"
    		android:layout_height="match_parent"
    		android:background="@android:color/white"
    		android:orientation="vertical">

    		<ImageView
        		android:id="@+id/imageView1"
        		android:layout_width="match_parent"
        		android:layout_height="0dp"
        		android:layout_weight="9"
        		android:src="@drawable/place_holder" />

    		<Button
        		android:id="@+id/button1"
        		android:layout_width="match_parent"
        		android:layout_height="0dp"
        		android:layout_weight="1"
        		android:text="Load Image" />

    		</LinearLayout>
    
    
    
	Step 2:  In Activity java file, Create reference for image view and button view.
    
	Step 3:  Copy the following code in activity class scope.
    		ImageView imageView;
		Button loadBtn;
    
    
	Step 4:  In OnCreate Method map the reference variable to object
    

	Step 5:  Copy the following code in onCreate Method
    		imageView = (ImageView) findViewById(R.id.imageView1);
		loadBtn = (Button) findViewById(R.id.button1);
    
    
	Step 6:  Create a Utils class (Right Click -> New -> Java Class)
    
	Step 7:  Copy the following code in newly created class
    		public class Utils {

    		public static InputStream openHttpConnection(String urlStr) {
        		InputStream in = null;
        		int resCode = -1;

        		try {
            		URL url = new URL(urlStr);
            		URLConnection urlConn = url.openConnection();

            		if (!(urlConn instanceof HttpURLConnection)) {
                		throw new IOException("URL is not an Http URL");
            		}

            		HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            		httpConn.setAllowUserInteraction(false);
            		httpConn.setInstanceFollowRedirects(true);
            		httpConn.setRequestMethod("GET");
            		httpConn.connect();

            		resCode = httpConn.getResponseCode();
            		if (resCode == HttpURLConnection.HTTP_OK) {
                		in = httpConn.getInputStream();
            		}
        		} catch (MalformedURLException e) {
            		e.printStackTrace();
        		} catch (IOException e) {
            		e.printStackTrace();
        		}
        		return in;
    		}
		}

    
    
	Step 8:  Create a custom Async class ImageLoaderAsyncTask . 
		(Right Click -> New -> Java Class)
    

	Step 9:  Copy the following code in newly created class
     		public class ImageLoaderAsyncTask extends AsyncTask<String, Integer, Bitmap> {

    		Context context;
    		ImageView imageView;
    		String url = "";


    		public ImageLoaderAsyncTask(Context context, String url, ImageView imageView) {
        		this.context = context;
        		this.url = url;
        		this.imageView = imageView;
   		 }

    		@Override
    		protected void onPreExecute() {
        		super.onPreExecute();
    		}

    		@Override
    		protected Bitmap doInBackground(String... params) {
        
        		return null;
    		}

    

    		@Override
    		protected void onPostExecute(Bitmap result) {
        		super.onPostExecute(result);
        		}


		}

    
	Step 10:  We need to download the image in background and convert the incoming input stream into bitmap.
    
	Step 11:  Copy the following code in doInBackground method
     		@Override
    		protected Bitmap doInBackground(String... params) {
    	
        		InputStream in = Utils.openHttpConnection(url);
        		Bitmap bmp = BitmapFactory.decodeStream(in);

        		return bmp;
    		}
    
    
	Step 12:  when the images is downloaded and converted into a bitmap object, we need to update it on the image view
    
	Step 13:  Copy the following code in onPostExecute method.
    		@Override
    		protected void onPostExecute(Bitmap result) {
        		super.onPostExecute(result);

        		if (result != null)
            		imageView.setImageBitmap(result);

       

    		}

    
	Step 13:  set Click handler on button.
    
	Step 14:  Execute the ImageLoaderAsyncTask in onClick event
    
	Step 15:  Paste this code in click handler
    		loadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImageLoaderAsyncTask(MainActivity.this, "http://iiitkota.forsklabs.in/appLogo.png", imageView).execute();
            }
        });
        
    Step 15 B: Add Internet permission in Android-Manifest file.
            <uses-permission android:name="android.permission.INTERNET"/>
        
    ## Progress Dialog
    
	Step 16:  To show the Progress bar during the downloading of image. we will use progress dialog class.
    
	Step 17:  Create a reference variable in class scope
        
		ProgressDialog pd;

    
    
	Step 18:  Since onPreExecute method is use to execute the task before the background thread. 
		so we will use this method to init our progress dialog and show on UI.
   		 @Override
    		protected void onPreExecute() {
        		super.onPreExecute();

        		pd = new ProgressDialog(context);
        		pd.setMessage("Loading Image..");
        		pd.setCancelable(false);
        		pd.show();
    		}
    
    
	Step 19:  Since OnPostExecute method is use to execute the task after the background thread. 
		so will use this method to hide the progress dialog.
     		@Override
    		protected void onPostExecute(Bitmap result) {
        		super.onPostExecute(result);

        		if (result != null)
            		imageView.setImageBitmap(result);

        		if (pd != null)
            		pd.dismiss();

    		}
    
    
	Step 20:  To show the update during the downloading process, we will use onProgressUpdate method.
		We will send update to this method from our background thread.

    Step 21:  Copy paste the following code in custom async task class
    
    		@Override
   		 protected Bitmap doInBackground(String... params) {
        		InputStream in = Utils.openHttpConnection(url);
        		Bitmap bmp = BitmapFactory.decodeStream(in);

        
        		for (int i = 1; 10 > i; i++) {
            		try {
                		onProgressUpdate(i);
                		Thread.sleep(500);
            		} catch (InterruptedException e) {
                		// TODO Auto-generated catch block
                		e.printStackTrace();
            		}
        		}
        		return bmp;
    		}
    
     		@Override
    		protected void onProgressUpdate(Integer... values) {
        		super.onProgressUpdate(values);

        
        		final int progress = values[0] * 10;

        		((Activity) context).runOnUiThread(new Runnable() {
            		@Override
            		public void run() {
                		pd.setMessage("Loading Image  " + progress + "%..");
            		}
        		});

    		}
    
    
	Step 22:  To avoid all hustle of create custom Async task for showing images we have a optimised third party library Aquery.
    
	Step 23:  Download link - 
       		https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/android-query/android-query.0.26.7.jar
    
	Extra Reading
        http://programmerguru.com/android-tutorial/tag/aquery/
    

	Step 24:  Download the library and copy the jar file in projectview -> app -> libs folder.
    
	Step 25:  Now right click on the library and select option 'add as library'.
                Add this to app module.
    
	Step 26:  now on the click handler
    
	Step 27:  Copy the following code on Click handler
    
    			@Override
			public void onClick(View v) {
				
				new AQuery(MainActivity.this).id(imageView).image("http://iiitkota.forsklabs.in/appLogo.png");
				
			}
    
    
  
2. Show data in Scroll View
	
	Step 1: Open project ReadingJsonFromInternet.
	
	Step 2:  Copy the following code in mail activity XML
	 
		<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:weightSum="10">

    <ScrollView
        android:id="@+id/scroll"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_above="@+id/button1"
        android:layout_weight="9"
        android:fillViewport="true">


        <LinearLayout
            android:id="@+id/scrollBase"
            android:orientation="vertical"
            android:layout_width="match_parent"
            android:layout_height="match_parent"></LinearLayout>
    </ScrollView>

    <Button
        android:id="@+id/button1"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:text="Show Raw JSON" />


</LinearLayout>



	Step 3:  Remove all the reference of text view and create reference for scroll base linear layout.
            ## Add this in class scope
            LinearLayout scrollBase;
    
            ## Add this in onCreate
            scrollBase = (LinearLayout)findViewById(R.id.scrollBase);

	Step 4: Modify the showFormattedJson method
    
            public void showFormattedJson(String raw_json) {

                ArrayList<FacultyWrapper> facultyWrapperArrayList = Utils.pasreLocalFacultyJson(raw_json);


                for (FacultyWrapper facultyWrapper : facultyWrapperArrayList) {

                    TextView tv =  new TextView(MainActivity.this);
                    tv.setText(facultyWrapper.getFirst_name());
                    scrollBase.addView(tv);
                }

            }
    
    Step 5:  Remove the reference for LinearLayout. 
    
    Step 6 : Comment the showFormattedJson method. Also comment the calling from onPostExecute method.
	
	
	Step 7:  Copy the following code in mail activity XML
	 
		<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    		xmlns:tools="http://schemas.android.com/tools"
    		android:layout_width="match_parent"
    		android:layout_height="match_parent"
    		android:orientation="vertical"
    		android:weightSum="10">

   		<ListView
        		android:id="@+id/listView1"
        		android:layout_width="match_parent"
        		android:layout_height="0dp"
        		android:layout_above="@+id/button1"
        		android:layout_weight="9">

    		</ListView>

    		<Button
        		android:id="@+id/button1"
        		android:layout_width="match_parent"
       			android:layout_height="0dp"
        		android:layout_weight="1"
        		android:text="Show JSON From Internet" />


		</LinearLayout>

	Step 8:  create reference for List view.
    
            ## Copy this code in Class scope
            ListView myListView;
    
            ## Copy this code in onCreate method Scop
            myListView = (ListView)findViewById(R.id.listView1); 
    
	

	Step 9:  Create a  new XML layout for list view item in layout folder.
		(Right Click on layout -> new -> lauoyt file -> row_faculty_profile_list.xml)
	

	Step 10:  Design a single row in this XML file.
	
	Step 11:  Copy the following code in newly created file
		<?xml version="1.0" encoding="utf-8"?>
		<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    		android:layout_width="match_parent"
    		android:layout_height="match_parent"
    		android:orientation="vertical" >

    		<LinearLayout
        		android:layout_width="match_parent"
        		android:layout_height="wrap_content" >

        		<ImageView
           		android:id="@+id/profileIv"
            		android:layout_width="100dp"
            		android:layout_height="100dp"
            		android:scaleType="centerCrop" />

        		<LinearLayout
           		 android:layout_width="wrap_content"
            		android:layout_height="match_parent"
            		android:orientation="vertical" >

            		<TextView
                		android:id="@+id/nameTv"
                		android:layout_width="wrap_content"
                		android:layout_height="wrap_content"
                		android:text="Medium Text"
                		android:textAppearance="?android:attr/textAppearanceMedium" />

            		<TextView
                		android:id="@+id/departmentTv"
                		android:layout_width="wrap_content"
                		android:layout_height="wrap_content"
                		android:text="Medium Text"
                		android:textAppearance="?android:attr/textAppearanceMedium" />

            		<TextView
                		android:id="@+id/reserch_areaTv"
                		android:layout_width="wrap_content"
                		android:layout_height="wrap_content"
                		android:text="Medium Text"
                		android:textAppearance="?android:attr/textAppearanceMedium" />
        		</LinearLayout>
    		</LinearLayout>

		</LinearLayout>


	Step 12:  Now create Adapter for list view.
	
	Step 13:  Create new package 'Adapter'. (Right Click -> New -> package)
	

	Step 14:  Create new class FacultyListAdapter in newly create adapter pkg. 
			(Right Click -> new -> java class) and extend it with BaseAdapter.
	

	Step 15:  Click on Red Light bulb on top left,which implements BaseAdapter methods.
    
    Step 16: Download link - 
       		https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/android-query/android-query.0.26.7.jar
    

	Step 17:  Download the library and copy the jar file in projectview -> app -> libs folder.
    
	Step 18:  Now right click on the library and select option 'add as library'.
                Add this to app module.
	
	Step 19:  Create reference variable for context,inflater,Aquery and arrayList of faculty wrapper.
	
	Step 20:  Copy the following code in adapter class scope
			Context context;
			ArrayList<FacultyWrapper> mFacultyDataList;
			LayoutInflater inflater;
			AQuery aq;

	Step 21:  Now Create Constructor for FacultyListAdapter
	
	Step 22:  Copy the following code in adapter class scope
			public FacultyListAdapter(Context context, ArrayList<FacultyWrapper> mFacultyDataList) {
				this.context = context;
				this.mFacultyDataList = mFacultyDataList;

				inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

				aq = new AQuery(context);
			}
	
	
	Step 23:  How many list item to be created at runtime is decided by getCount method
	
	Step 24:  Replace the following methods in class scope
			@Override
			public int getCount() {
				return mFacultyDataList.size();
			}
	
			@Override
			public Object getItem(int position) {
				return mFacultyDataList.get(position);
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

	
	Step 25:  getView() - This is the most important method in adapter class, 
		which is responsible for creating a single row view and populate it with data.
	
	Step 26:  In the layout XML we have to design the structure, now we need to attach it with data.
	
	Step 27:  Create a dummy view object using layout inflater.
	
	Step 28:  Replace the following code in getView method
			 @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_faculty_profile_list, null);
        }

        return null;
    }
		
	
	Step 29:  Create reference object and point towards the parent view object.
	
	Step 30:  Copy the following code in getView method
			ImageView profileIv = (ImageView) convertView.findViewById(R.id.profileIv);

				TextView nameTv = (TextView) convertView.findViewById(R.id.nameTv);
				TextView  departmentTv = (TextView) convertView.findViewById(R.id.departmentTv);
				TextView reserch_areaTv = (TextView) convertView.findViewById(R.id.reserch_areaTv);
		
	Step 31:  Now using current position, get the faculty wrapper object from the array list.
			FacultyWrapper obj = mFacultyDataList.get(position);
	
	
	Step 32:  Now populate the data from faculty wrapper to view reference object.
			AQuery temp_aq = aq.recycle(convertView);
				temp_aq.id(profileIv).image(obj.getPhoto(), true, true, 200, 0);

				nameTv.setText(obj.getFirst_name() + " " + obj.getLast_name());
				departmentTv.setText(obj.getDepartment());
				reserch_areaTv.setText(obj.getReserch_area());
		
	
	Step 33:  Since now the dummy view has the actual data, return the newly created view.
    
            ## Add this code in the end of getView method
            return convertView;
    
	
	Step 34:  Now in main activity class, Create an object of this adapter and set it to the list view.	
	
	Step 35:  Copy the following method in MainActivity class scope.
			public void showJsonInListView(String raw_json){
        		ArrayList<FacultyWrapper> facultyWrapperArrayList = Utils.pasreLocalFacultyJson(raw_json);
        
        		FacultyListAdapter adapter = new FacultyListAdapter(context,facultyWrapperArrayList);
        		myListView.setAdapter(adapter);
   		 }
	
	Step 36:  In onpost of CustomAsyncTask class we need to populate the list view. 
		so we call the method showJsonInListView to create adapter and set it to list view.
	
	Step 37:  Copy the following method in CustomAsyncTask class
	 		@Override
        		protected void onPostExecute(String result) {
            		super.onPostExecute(result);

            		Toast.makeText(context, "onPostExecute", Toast.LENGTH_SHORT).show();

            		showJsonInListView(result);
        		}
                
                
    Step 38: ViewHolder pattern 
    
            ## Copy this code in FacultyListAdapter class scope
            ViewHolder holder;
    
    Step 39 :
            ## Replace the following code in FacultyListAdapter
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_faculty_profile_list, null);

            //init the View Holder object,when a new view is created for the first time.
            holder =  new ViewHolder(convertView);

            //Save this holder using the view tags
            convertView.setTag(holder);
        }else {
            //we need to retrive the view holder object from the saved tags
            //this is called when the view is taken from the recycled views
            //this save lot of processing time for findViewByid
            holder =  (ViewHolder) convertView.getTag();
        }

//        ImageView profileIv = (ImageView) convertView.findViewById(R.id.profileIv);
//
//        TextView nameTv = (TextView) convertView.findViewById(R.id.nameTv);
//        TextView  departmentTv = (TextView) convertView.findViewById(R.id.departmentTv);
//        TextView reserch_areaTv = (TextView) convertView.findViewById(R.id.reserch_areaTv);

        FacultyWrapper obj = mFacultyDataList.get(position);

        AQuery temp_aq = aq.recycle(convertView);
        //Now you have to take view referece object from the view holder object. Which hold all the referece for you.
        temp_aq.id(holder.profileIv).image(obj.getPhoto(), true, true, 200, 0);


        //Now you have to take view referece object from the view holder object. Which hold all the referece for you.
        holder.nameTv.setText(obj.getFirst_name() + " " + obj.getLast_name());
        holder.departmentTv.setText(obj.getDepartment());
        holder.reserch_areaTv.setText(obj.getReserch_area());


        return convertView;
    }


    public static class ViewHolder {
        ImageView profileIv;
        TextView nameTv;
        TextView departmentTv;
        TextView reserch_areaTv;

        public ViewHolder(View convertView) {
            profileIv = (ImageView) convertView.findViewById(R.id.profileIv);

            nameTv = (TextView) convertView.findViewById(R.id.nameTv);
            departmentTv = (TextView) convertView.findViewById(R.id.departmentTv);
            reserch_areaTv = (TextView) convertView.findViewById(R.id.reserch_areaTv);
        }
    }
	